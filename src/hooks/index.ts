import { useGlobSetting } from "./modules/setting.tsx"
import { useMatchRouter } from "./modules/use-match-router.tsx"
import { useTabs } from "./modules/use-tabs.tsx"
export { useGlobSetting, useMatchRouter, useTabs }
