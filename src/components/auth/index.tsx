import React from "react"
import { isAuth } from "./is-auth.ts"
const AuthButton: React.FC<{ authCode: string; children: React.ReactElement }> = ({
  authCode,
  children
}) => {
  if (isAuth(authCode)) {
    return children
  }
  return null
}
export default AuthButton
