import { useMatches } from "react-router-dom"
export const isAuth = (authCode: string) => {
  const matches = useMatches()
  if (!authCode) return false
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  const authList = matches.at(-1).handle?.actions
  return authList?.includes(authCode)
}
