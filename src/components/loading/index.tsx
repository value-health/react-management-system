import NProgress from "nprogress"
import React, { useEffect } from "react"
import { Spin } from "antd"
export const Loading: React.FC = () => {
  useEffect(() => {
    NProgress.start()

    return () => {
      NProgress.done()
    }
  }, [])
  return (
    <div>
      <Spin />
    </div>
  )
}
