import React from "react"
import { Button } from "antd"
import AuthButton from "@/components/auth"
const SystemRole: React.FC = () => {
  return (
    <div>
      <AuthButton authCode={"CREATE"}>
        <Button type={"primary"}>新建</Button>
      </AuthButton>
      <AuthButton authCode={"DETAIL"}>
        <Button type={"primary"}>修改</Button>
      </AuthButton>
      <AuthButton authCode={"DELETE"}>
        <Button type={"primary"}>查看</Button>
      </AuthButton>
      <AuthButton authCode={"DETAIL"}>
        <Button type={"primary"} danger>
          删除
        </Button>
      </AuthButton>
    </div>
  )
}

export default SystemRole
