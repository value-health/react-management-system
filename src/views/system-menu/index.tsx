import React from "react"
import { Button } from "antd"
import AuthButton from "@/components/auth"
const SystemMenu: React.FC = () => {
  return (
    <div>
      <AuthButton authCode={"CREATE"}>
        <Button type={"primary"}>新建</Button>
      </AuthButton>

      <Button type={"primary"}>删除</Button>
      <Button type={"primary"}>修改</Button>
      <Button type={"primary"} danger>
        删除
      </Button>
    </div>
  )
}

export default SystemMenu
