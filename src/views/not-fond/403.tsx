import React from "react"
import { Button, Result } from "antd"

const NotFond403: React.FC = () => (
  <Result
    status="error"
    title="已过期"
    subTitle="您的登录已过期，请重新登录"
    extra={[
      // eslint-disable-next-line react/jsx-key
      <Button type="primary">
        <a href="/login">回到首页</a>
      </Button>
    ]}
  />
)

export default NotFond403
