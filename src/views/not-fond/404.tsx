import { Button, Result } from "antd"
import React from "react"
import { Link } from "react-router-dom"

const NotFond404: React.FC = () => (
  <Result
    status="error"
    title="出错了"
    subTitle="我们正在努力修复中，请稍后再试。"
    extra={[
      // eslint-disable-next-line react/jsx-key
      <Button type="primary">
        <Link to="/">首页</Link>
      </Button>
    ]}
  />
)

export default NotFond404
