import React from "react"
import { Button, Result } from "antd"

const NotFond401: React.FC = () => (
  <Result
    status="warning"
    title="无权限"
    subTitle="抱歉您没有权限，请联系管理员！"
    extra={[
      // eslint-disable-next-line react/jsx-key
      <Button type="primary">
        <a href="/public">回到首页</a>
      </Button>
    ]}
  />
)

export default NotFond401
