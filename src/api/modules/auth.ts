import request from "@/utils/http/request"

export interface MenuModel {
  _id?: string
  subtitle?: string
  name?: string
  icon?: string
  path?: string
  children?: MenuModel[]
}

export const getMenusList = () => request.get<MenuModel[]>("/api/auth/menu-list")
