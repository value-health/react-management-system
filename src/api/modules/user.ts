import request from "@/utils/http/request.ts"
import { UserInfo } from "@/types"
import { BasicResponseModel } from "@/types"

export interface LoginDTO {
  username?: string
  password?: string
  phone?: string
  captcha?: string
}

export const loginTo = (data: LoginDTO) => request.post<BasicResponseModel>("/api/login", data)

export const getRefreshToken = (data: { refreshToken: string }) =>
  request.post<BasicResponseModel>("/api/refresh-token", data)

export const getCaptcha = () => request.get<BasicResponseModel>("/api/captcha")

export const loginOut = () => request.get<BasicResponseModel>("/api/login-out")

export const getUserRequest = (params: { _id: string }) =>
  request.get<UserInfo>("/api/user/list", { params })
