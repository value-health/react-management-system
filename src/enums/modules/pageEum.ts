export enum PageEnum {
  // Basic routing// Basic routing
  BASE_HOME = "/home",
  // Login route
  BASE_LOGIN = "/login",
  // Register route
  BASE_REGISTER = "/register",
  // Not found page route
  BASE_NOTFOUND_401 = "/401",
  BASE_NOTFOUND_403 = "/403",
  BASE_NOTFOUND_404 = "/404"
  // Server error page route
}
