import React from "react"
import { MenuType } from "@/enums"
export interface RouteObject {
  children?: RouteObject[]
  element?: React.ReactNode
  index?: boolean
  path?: string
  auth?: boolean
}

export const MenuTypeName = {
  [MenuType.DIRECTORY.toString()]: "目录",
  [MenuType.MENU.toString()]: "菜单",
  [MenuType.BUTTON.toString()]: "按钮"
}
