export interface BasicResponseModel<T = any> {
  code: number
  message: string
  status: string
  result?: T
}
