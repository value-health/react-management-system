import React, { Suspense } from "react"
import { Loading } from "@/components/loading"

const Content: React.FC<any> = ({ children }) => {
  return (
    <div>
      <div className="layout-content">
        <Suspense fallback={<Loading />}>{children}</Suspense>
      </div>
    </div>
  )
}

export default Content
