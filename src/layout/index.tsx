import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons"
import { ProLayout, ProBreadcrumb } from "@ant-design/pro-components"
import { Switch } from "antd"
import { components } from "@/config/routes.tsx"
import { TabsLayout } from "./tabs-layout"
import Content from "./content"
import React, { useEffect, useState, lazy } from "react"
import { replaceRoutes, basicRouter } from "@/router/index.tsx"
import { useNavigate } from "react-router-dom"
import { useGlobalStore } from "@/store"
import { useRequest } from "ahooks"
import { getUserRequest } from "@/api"
import { Menu } from "@/types"
import { getAllPermissions } from "@/utils/index.ts"
import { useMatchRouter } from "@/hooks"

const BasicLayout: React.FC = () => {
  const { refreshToken, userId } = useGlobalStore()
  const [menuList, setMenuList] = useState([])
  const [pathname, setPathname] = useState("/dashboard/main")
  const [collapsed, setCollapsed] = useState<boolean>(true)
  const matchRoute = useMatchRouter()
  const navigate = useNavigate()
  const { data: getMenuList, run: getMenusListAction } = useRequest(getUserRequest, {
    manual: true
  })

  useEffect(() => {
    if (!refreshToken && !userId) {
      navigate("/login")
      return
    }
    const fetchMenusList = async () => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      await getMenusListAction({ _id: userId })
    }
    fetchMenusList()
  }, [refreshToken, getMenusListAction, navigate, userId])

  useEffect(() => {
    if (!getMenuList) return
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    const { permission = [] } = getMenuList[1].result[0]
    const menuGroup = permission.reduce<Record<string, Menu[]>>((prev, menu) => {
      if (!menu.parentId) {
        return prev
      }
      if (!prev[menu.parentId]) {
        prev[menu.parentId] = []
      }

      prev[menu.parentId].push(menu)
      return prev
    }, {})
    const routes: Menu[] = []

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    getMenuList[1].menus = formatMenus(
      permission.filter((o) => !o.parentId),
      menuGroup,
      routes
    )

    replaceRoutes("*", [
      ...routes.map((menu) => {
        return {
          path: `/*${menu.path}`,
          Component: menu.component ? lazy(components[menu.component]) : null,
          name: menu.name,
          id: `${menu._id}`,
          handle: {
            parentPaths: menu.parentPaths,
            actions: menu.roles && getAllPermissions(menu.roles),
            path: menu.path,
            name: menu.name,
            icon: menu.icon
          }
        }
      })
    ])
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    setMenuList(getMenuList[1].menus)
    setPathname(location.pathname)
    basicRouter.navigate(`${location.pathname}${location.search}`, {
      replace: true
    })
  }, [getMenuList, setMenuList, matchRoute])

  const onMenuItemClick = (item: any) => {
    setPathname(item?.path)
    navigate(item.path)
  }

  const formatMenus = (
    menus: Menu[],
    menuGroup: Record<string, Menu[]>,
    routes: Menu[],
    parentMenu?: Menu
  ): Menu[] => {
    return menus.map((menu: any) => {
      const children = menuGroup[menu?._id]
      const parentPaths = parentMenu?.parentPaths || []
      const path = (parentMenu ? `${menu.path}` : menu.path) || ""
      routes.push({ ...menu, path, parentPaths })
      return {
        ...menu,
        path,
        parentPaths,
        children: children?.length
          ? formatMenus(children, menuGroup, routes, {
              ...menu,
              parentPaths: [...parentPaths, path || ""].filter((o) => o)
            })
          : undefined
      }
    })
  }

  return (
    <div
      id="test-pro-layout"
      style={{
        height: "100vh"
      }}
    >
      <ProLayout
        title={"指校针管理系统"}
        route={{
          path: "/",
          children: menuList || []
        }}
        layout="mix"
        token={{
          header: {},
          sider: {
            colorMenuBackground: "#0f0f0f",
            colorTextMenu: "#595959",
            colorTextMenuSelected: "#fff",
            colorTextMenuItemHover: "#fff",
            colorTextMenuActive: "rgba(65, 120, 243, 1)",
            colorBgMenuItemSelected: "rgba(65, 120, 243, 1)"
          }
        }}
        location={{
          pathname
        }}
        onCollapse={setCollapsed}
        menuItemRender={(item, dom) => <div onClick={() => onMenuItemClick(item)}>{dom}</div>}
        collapsed={collapsed}
        avatarProps={{
          src: "https://gw.alipayobjects.com/zos/antfincdn/efFD%24IOql2/weixintupian_20170331104822.jpg",
          size: "small",
          title: "七妮妮"
        }}
        actionsRender={() => {
          // eslint-disable-next-line react/prop-types
          return [
            // eslint-disable-next-line react/jsx-key
            <div>
              <Switch checkedChildren={"默认"} unCheckedChildren={"暗黑"} defaultChecked />
            </div>
          ]
        }}
        fixSiderbar={true}
        collapsedButtonRender={false}
        headerContentRender={() => {
          return (
            <div
              style={{
                display: "flex",
                alignItems: "center"
              }}
            >
              <div
                onClick={() => setCollapsed(!collapsed)}
                style={{
                  cursor: "pointer",
                  fontSize: "16px",
                  margin: "0 20px"
                }}
              >
                {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
              </div>
              <ProBreadcrumb />
            </div>
          )
        }}
      >
        {/*<PageContainer ghost breadcrumbRender={false}>*/}

        {/*</PageContainer>*/}
        <Content>
          <TabsLayout />
        </Content>
      </ProLayout>
    </div>
  )
}

export default BasicLayout
