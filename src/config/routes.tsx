export const modules = import.meta.glob("@/views/**/index.tsx")
export const components = Object.keys(modules).reduce((prev, cur) => {
  prev[cur.replace(/^\/src\/views\/(.*)\/index\.tsx$/, "$1")] = modules[cur]
  return prev
}, {})
