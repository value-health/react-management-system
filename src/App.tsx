import { ConfigProvider, App as AntdApp, theme } from "antd"
import enUS from "antd/locale/en_US"
import zhCN from "antd/locale/zh_CN"
import { Suspense } from "react"
import { Loading } from "@/components/loading"
import Router from "@/router"
import { useGlobalStore } from "@/store"
import "./app.less"
function App() {
  const { darkMode, lang } = useGlobalStore()
  return (
    <ConfigProvider
      theme={{
        algorithm: !darkMode ? theme.darkAlgorithm : theme.defaultAlgorithm
      }}
      locale={lang === "zh" ? zhCN : enUS}
      componentSize="large"
    >
      <Suspense fallback={<Loading />}>
        <AntdApp>
          <Router />
        </AntdApp>
      </Suspense>
    </ConfigProvider>
  )
}
export default App
