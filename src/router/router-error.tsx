import { useRouteError } from "react-router-dom"

const RouterErrorElement = () => {
  throw useRouteError()
}

export default RouterErrorElement
